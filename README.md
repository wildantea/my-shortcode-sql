UPDATE
    laporan_dapil pd INNER JOIN laporan_dapil pd2 ON
    (pd.id=pd2.id)
    left join villages v on pd.id_desa=v.id
inner join districts d on v.district_id=d.id
inner join regencies r on d.regency_id=r.id
inner join provinces p on r.province_id=p.id
SET pd.id_provinsi = p.id;